-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2017 at 07:25 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci-admin-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'Admin', 'admin', 'admin', 'admin@admin.com', '12345', '$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS', 1, 1, 1, '', '2017-09-29 10:09:44', '2017-10-17 10:10:55'),
(15, 'demo3', 'test', 'test', 'test2@gmai.com', '12345', '$2y$10$1rzqcv/3K1EUlF2J1zgDiuS3nIN6/YLZ1ABLuMiZysFDTCkcyJuTC', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:04'),
(16, 'demo4', 'test', 'test', 'test3@gmai.com', '12345', '$2y$10$a7GTZPFtVK05ldiI9gYfgunfQHZfalAO/syWItjnhiyahXbfNcWoy', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:12'),
(17, 'demo5', 'test', 'test', 'test4@gmai.com', '12345', '$2y$10$iQW7TKdrHNnl3jDtjYBadOxTuWRBdBcZhk2lxZ2lrIX1M4uJu6pFO', 4, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:17'),
(18, 'demo6', 'test', 'test', 'test5@gmai.com', '12345', '$2y$10$UEkLtiOP8Lt13vwC8KXsKOOJMnWukPPP7L/NJIpFn49rQKuA6oXD6', 1, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:24'),
(19, 'demo7', 'test', 'test', 'test6@gmai.com', '12345', '$2y$10$GSVyEzAbMjdhONCVPevbGOMAjzSGpPk62pg58W8DtG4PCSdTF5Ooy', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:30'),
(20, 'demo8', 'test', 'test', 'test7@gmai.com', '12345', '$2y$10$6415cSa21VXXyD3vsaPwyepPyaDpPMgOJkPbZMt/AtKvNx5hRxbLy', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:39'),
(21, 'demo9', 'test', 'test', 'test8@gmai.com', '12345', '$2y$10$.4.73sLxhwRHYZoSxbFTMu/iaKHgDYJqz9Lx3js6dmlJxZMuPG8AG', 1, 0, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:47'),
(22, 'demo10', 'test', 'test', 'test9@gmai.com', '12345', '$2y$10$.Rr.YeXQ/M4QJ031O3r2k.Tc2ztOvRfCsG2EjYUxY.KZ96WjwR57O', 1, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:53'),
(23, 'demo11', 'test', 'test', 'test10@gmai.com', '12345', '$2y$10$cyuI4z5NWt/S1X9Z3vOgWuDKm/ZJGOPy72kZHrrOP0TisXobhaIri', 2, 1, 0, '', '2017-10-12 07:10:10', '2017-10-17 11:10:00'),
(24, 'demo12', 'test', 'test', 'test1@gmai.com', '12345', '$2y$10$JlHihhr5LiGtx7rwZYbLLefdmi3JACN/yDkoJKWWESwELAGFeWOWe', 4, 1, 0, '', '2017-10-12 07:10:10', '2017-10-13 10:10:22'),
(25, 'rizwan', 'rizwan', 'shahid', 'rizwan@gmail.com', '12345', '$2y$10$HdgLtow.ZkitfcEj9rs.9e4yVRFl4LDoXiqonceKnyaXBslPWMh5q', 4, 1, 0, '', '2017-10-13 09:10:37', '2017-10-17 11:10:56'),
(27, 'wwe', 'wwe', 'test', 'test1@gmail.com', '123456', '$2y$10$d6hK4XEEg5.igyWFye5XrO3S813HDZy3Tr5QwrCPSgr2aVQ2eOD8y', 1, 1, 0, '', '2017-10-17 10:10:50', '2017-10-17 10:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_groups`
--

CREATE TABLE `ci_user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_user_groups`
--

INSERT INTO `ci_user_groups` (`id`, `group_name`) VALUES
(1, 'System Admin'),
(2, 'Service Center Admin'),
(4, 'Service Center User');

-- --------------------------------------------------------

--
-- Table structure for table `t_issue_reason`
--

CREATE TABLE `t_issue_reason` (
  `id` int(11) NOT NULL,
  `issue_reason` varchar(255) NOT NULL,
  `trans_type` varchar(1) NOT NULL DEFAULT 'I'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_issue_reason`
--

INSERT INTO `t_issue_reason` (`id`, `issue_reason`, `trans_type`) VALUES
(1, 'เบิกใช้งาน', 'i'),
(2, 'ส่งซ่อม / ชำรุด', 'i'),
(3, 'ปรับออก จากการตวจนับ', 'i');

-- --------------------------------------------------------

--
-- Table structure for table `t_item`
--

CREATE TABLE `t_item` (
  `id_item` int(13) NOT NULL,
  `item_barcode` varchar(13) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `item_um_code` int(5) NOT NULL,
  `qty_stock_minimun` int(11) NOT NULL,
  `item_category_id` int(5) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `item_type_id` int(5) NOT NULL,
  `item_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_item`
--

INSERT INTO `t_item` (`id_item`, `item_barcode`, `item_description`, `item_code`, `item_um_code`, `qty_stock_minimun`, `item_category_id`, `item_image`, `item_type_id`, `item_status`) VALUES
(1, '1122334455', 'Paper BOX 300', 'BOX300', 1, 7, 1, '3d947-box300.jpg', 2, 1),
(2, '2255889933', 'Paper Box 500', 'BOX500', 1, 5, 1, '0b0f8-box300.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_item_category`
--

CREATE TABLE `t_item_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_item_category`
--

INSERT INTO `t_item_category` (`id`, `category_name`) VALUES
(1, 'BOX Packaging'),
(2, 'COMPUTER'),
(3, 'UNIFORM');

-- --------------------------------------------------------

--
-- Table structure for table `t_item_type`
--

CREATE TABLE `t_item_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_item_type`
--

INSERT INTO `t_item_type` (`id`, `type_name`) VALUES
(1, 'Single Issue'),
(2, 'Batch Issue');

-- --------------------------------------------------------

--
-- Table structure for table `t_item_unit`
--

CREATE TABLE `t_item_unit` (
  `id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `id_unit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_receive_reason`
--

CREATE TABLE `t_receive_reason` (
  `id` int(11) NOT NULL,
  `receive_reason` varchar(255) NOT NULL,
  `trans_type` varchar(1) NOT NULL DEFAULT 'R'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_receive_reason`
--

INSERT INTO `t_receive_reason` (`id`, `receive_reason`, `trans_type`) VALUES
(1, 'รับเข้า', 'r'),
(2, 'รับคืน จากการส่งซ่อม', 'r'),
(3, 'ปรับเข้า จากการตรวจนับ', 'r');

-- --------------------------------------------------------

--
-- Table structure for table `t_service_center`
--

CREATE TABLE `t_service_center` (
  `id_center` int(5) NOT NULL,
  `center_code` varchar(50) NOT NULL,
  `center_name` varchar(255) NOT NULL,
  `center_address` varchar(255) NOT NULL,
  `center_map` varchar(255) NOT NULL,
  `center_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_service_center`
--

INSERT INTO `t_service_center` (`id_center`, `center_code`, `center_name`, `center_address`, `center_map`, `center_status`) VALUES
(1, 'RMT', 'Service Center RAMA3', 'Rama 3 Road Juamjuree Square', 'abc62-map.jpg', 1),
(2, 'APD', 'Service Center Duanmuang', '12/454 DM', '2b5fd-map.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE `t_status` (
  `status_id` int(1) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_status`
--

INSERT INTO `t_status` (`status_id`, `status`) VALUES
(1, 'Active'),
(2, 'Non Active');

-- --------------------------------------------------------

--
-- Table structure for table `t_transaction`
--

CREATE TABLE `t_transaction` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_type` varchar(1) NOT NULL,
  `type_reason` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `trans_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_transaction`
--

INSERT INTO `t_transaction` (`id`, `item_id`, `transaction_type`, `type_reason`, `qty`, `trans_date`) VALUES
(37, 1, 'r', 1, 10, '2017-11-02 14:46:35'),
(38, 1, 'i', 1, -5, '2017-11-02 15:04:05'),
(39, 2, 'r', 1, 100, '2017-11-03 12:43:45'),
(40, 1, 'r', 3, 123, '2017-11-03 12:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `t_unit`
--

CREATE TABLE `t_unit` (
  `item_um_code` int(5) NOT NULL,
  `unit` varchar(200) NOT NULL,
  `base_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_unit`
--

INSERT INTO `t_unit` (`item_um_code`, `unit`, `base_quantity`) VALUES
(1, 'BOX', 1),
(2, 'PCS', 1),
(3, 'DOZ', 12),
(4, 'PACK 30 PCS', 30);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_lower_stock`
-- (See below for the actual view)
--
CREATE TABLE `v_item_lower_stock` (
`item_id` int(11)
,`item_description` varchar(255)
,`qty_balance` decimal(32,0)
,`qty_stock_minimun` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_on_hand`
-- (See below for the actual view)
--
CREATE TABLE `v_item_on_hand` (
`item_id` int(11)
,`item_description` varchar(255)
,`qty_balance` decimal(32,0)
,`qty_stock_minimun` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_transaction`
-- (See below for the actual view)
--
CREATE TABLE `v_item_transaction` (
`id` int(11)
,`item_id` int(11)
,`transaction_type` varchar(1)
,`type_reason` int(11)
,`qty` int(11)
,`trans_date` datetime
,`item_barcode` varchar(13)
,`item_description` varchar(255)
,`item_code` varchar(20)
,`item_um_code` int(5)
,`qty_stock_minimun` int(11)
,`item_image` varchar(255)
,`unit` varchar(200)
,`reason` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_transaction_type`
-- (See below for the actual view)
--
CREATE TABLE `v_transaction_type` (
`trans_type` varchar(1)
,`id` int(11)
,`issue_reason` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `v_item_lower_stock`
--
DROP TABLE IF EXISTS `v_item_lower_stock`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_lower_stock`  AS  select `v_item_on_hand`.`item_id` AS `item_id`,`v_item_on_hand`.`item_description` AS `item_description`,`v_item_on_hand`.`qty_balance` AS `qty_balance`,`v_item_on_hand`.`qty_stock_minimun` AS `qty_stock_minimun` from `v_item_on_hand` where (`v_item_on_hand`.`qty_stock_minimun` >= `v_item_on_hand`.`qty_balance`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_on_hand`
--
DROP TABLE IF EXISTS `v_item_on_hand`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_on_hand`  AS  select `t1`.`item_id` AS `item_id`,`t2`.`item_description` AS `item_description`,sum(`t1`.`qty`) AS `qty_balance`,`t2`.`qty_stock_minimun` AS `qty_stock_minimun` from (`t_transaction` `t1` left join `t_item` `t2` on((`t1`.`item_id` = `t2`.`id_item`))) group by `t1`.`item_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_transaction`
--
DROP TABLE IF EXISTS `v_item_transaction`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_transaction`  AS  select `t1`.`id` AS `id`,`t1`.`item_id` AS `item_id`,`t1`.`transaction_type` AS `transaction_type`,`t1`.`type_reason` AS `type_reason`,`t1`.`qty` AS `qty`,`t1`.`trans_date` AS `trans_date`,`t2`.`item_barcode` AS `item_barcode`,`t2`.`item_description` AS `item_description`,`t2`.`item_code` AS `item_code`,`t2`.`item_um_code` AS `item_um_code`,`t2`.`qty_stock_minimun` AS `qty_stock_minimun`,`t2`.`item_image` AS `item_image`,`t3`.`unit` AS `unit`,`t4`.`issue_reason` AS `reason` from (((`t_transaction` `t1` left join `t_item` `t2` on((`t1`.`item_id` = `t2`.`id_item`))) left join `t_unit` `t3` on((`t2`.`item_um_code` = `t3`.`item_um_code`))) left join `v_transaction_type` `t4` on(((`t1`.`transaction_type` = `t4`.`trans_type`) and (`t1`.`type_reason` = `t4`.`id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_transaction_type`
--
DROP TABLE IF EXISTS `v_transaction_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_transaction_type`  AS  select `t1`.`trans_type` AS `trans_type`,`t1`.`id` AS `id`,`t1`.`issue_reason` AS `issue_reason` from `t_issue_reason` `t1` union all select `t2`.`trans_type` AS `trans_type`,`t2`.`id` AS `id`,`t2`.`receive_reason` AS `receive_reason` from `t_receive_reason` `t2` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_issue_reason`
--
ALTER TABLE `t_issue_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_item`
--
ALTER TABLE `t_item`
  ADD PRIMARY KEY (`id_item`,`item_code`);

--
-- Indexes for table `t_item_category`
--
ALTER TABLE `t_item_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_item_type`
--
ALTER TABLE `t_item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_item_unit`
--
ALTER TABLE `t_item_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_receive_reason`
--
ALTER TABLE `t_receive_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_service_center`
--
ALTER TABLE `t_service_center`
  ADD PRIMARY KEY (`id_center`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `t_transaction`
--
ALTER TABLE `t_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_unit`
--
ALTER TABLE `t_unit`
  ADD PRIMARY KEY (`item_um_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_issue_reason`
--
ALTER TABLE `t_issue_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_item`
--
ALTER TABLE `t_item`
  MODIFY `id_item` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_item_category`
--
ALTER TABLE `t_item_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_item_type`
--
ALTER TABLE `t_item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_item_unit`
--
ALTER TABLE `t_item_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_receive_reason`
--
ALTER TABLE `t_receive_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_service_center`
--
ALTER TABLE `t_service_center`
  MODIFY `id_center` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `status_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_transaction`
--
ALTER TABLE `t_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `t_unit`
--
ALTER TABLE `t_unit`
  MODIFY `item_um_code` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
