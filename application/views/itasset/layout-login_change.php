<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</head>
<body>
<div class = "container">
	<div class="wrapper" style="padding:5%;";>
		<form  action="<?php echo base_url('itasset/change_asset/check_login'); ?>" 
        method="post" name="Login_Form" class="form-signin">   
            <p style="text-align:center;">
            <img src="<?= base_url() ?>assets/images/logo-dhl.png" alt="qr download app" style="width:35%;text-align:center;">
            </p>    
        
		    <h3 class="form-signin-heading" style="text-align:center;">Please Login !</h3>
			  <hr class="colorgraph">
              <br>
              Asset qr code to change
              <br>
              <input type="text" class="form-control" name="show_qr_code" value="<?php echo $qr_code_id;?>" readonly />
              <input type="hidden" class="form-control" name="qr_code_id" value="<?php echo $qr_code_id;?>" />
              
			  <hr>
              username <br>
			  <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
              <br>
              password <br>
			  <input type="password" class="form-control" name="password" placeholder="Password" required=""/>     		  
              <br>
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Login</button>  			
              
		</form>			
	</div>
</div>
</body>
</html>