<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</head>
<body>
<div class = "container">
	<div class="wrapper" style="padding:5%;";>
		<form  action="<?php echo base_url('api/qr_scan/scan?qr_code_id='.$qr_code_id.''); ?>" 
        method="post" name="Login_Form" class="form-signin">       
		    <h3 class="form-signin-heading" style="text-align:center;"><?php echo $msg;?></h3>
			  <hr class="colorgraph">
              
              <br>
			 
			  <button class="btn btn-lg btn-danger btn-block"  name="Submit" value="Login" type="Submit">Back to Asset Information</button>  			
              
		</form>			
	</div>
</div>
</body>
</html>