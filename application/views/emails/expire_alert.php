<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8" />

  <title>Email Alert from DHL QRCode Inventory System</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body>

<h3>Asset List will be Expire in 0-30 days</h3>


<div class="asset-list">
        
        <TR>
        <h3>
        <TD width="10%">Expire Date</TD>
        <TD width="10%">Days to Expire</TD>
        <TD width="20%">Computer Name</TD>
        <TD width="10%">Serial</TD>
        <TD width="10%">Asset Code Number</TD>
        <TD width="10%">Location</TD>
        <TD width="10%">Vendor</TD>
        <TD width="10%">Model</TD>
        
        </h3>
        </TR>

        <?php foreach($asset_expired_1 as $row): ?>
        <TR>
        <h3>
        <TD width="10%"><?= $row['expire_date']; ?></TD>
        <TD width="10%"><?= $row['days_to_expire']; ?></TD>
        <TD width="20%"><?= $row['asset_name']; ?></TD>
        <TD width="10%"><?= $row['serial_number']; ?></TD>
        <TD width="10%"><?= $row['asset_code_number']; ?></TD>
        <TD width="10%"><?= $row['location']; ?></TD>
        <TD width="10%"><?= $row['vendor_name']; ?></TD>
        <TD width="10%"><?= $row['model']; ?></TD>

        </TR>
        </h3>
        <?php endforeach; ?>

</div>


<h3>Asset List will be Expire in 31-60 days</h3>


<div class="asset-list">
        
        <TR>
        <h3>
        <TD width="10%">Expire Date</TD>
        <TD width="10%">Days to Expire</TD>
        <TD width="20%">Computer Name</TD>
        <TD width="10%">Serial</TD>
        <TD width="10%">Asset Code Number</TD>
        <TD width="10%">Location</TD>
        <TD width="10%">Vendor</TD>
        <TD width="10%">Model</TD>
        
        </h3>
        </TR>

        <?php foreach($asset_expired_2 as $row): ?>
        <TR>
        <h3>
        <TD width="10%"><?= $row['expire_date']; ?></TD>
        <TD width="10%"><?= $row['days_to_expire']; ?></TD>
        <TD width="20%"><?= $row['asset_name']; ?></TD>
        <TD width="10%"><?= $row['serial_number']; ?></TD>
        <TD width="10%"><?= $row['asset_code_number']; ?></TD>
        <TD width="10%"><?= $row['location']; ?></TD>
        <TD width="10%"><?= $row['vendor_name']; ?></TD>
        <TD width="10%"><?= $row['model']; ?></TD>

        </TR>
        </h3>
        <?php endforeach; ?>

</div>


<h3>Asset List will be Expire in 61-90 days</h3>


<div class="asset-list">
        
        <TR>
        <h3>
        <TD width="10%">Expire Date</TD>
        <TD width="10%">Days to Expire</TD>
        <TD width="20%">Computer Name</TD>
        <TD width="10%">Serial</TD>
        <TD width="10%">Asset Code Number</TD>
        <TD width="10%">Location</TD>
        <TD width="10%">Vendor</TD>
        <TD width="10%">Model</TD>
        
        </h3>
        </TR>

        <?php foreach($asset_expired_3 as $row): ?>
        <TR>
        <h3>
        <TD width="10%"><?= $row['expire_date']; ?></TD>
        <TD width="10%"><?= $row['days_to_expire']; ?></TD>
        <TD width="20%"><?= $row['asset_name']; ?></TD>
        <TD width="10%"><?= $row['serial_number']; ?></TD>
        <TD width="10%"><?= $row['asset_code_number']; ?></TD>
        <TD width="10%"><?= $row['location']; ?></TD>
        <TD width="10%"><?= $row['vendor_name']; ?></TD>
        <TD width="10%"><?= $row['model']; ?></TD>

        </TR>
        </h3>
        <?php endforeach; ?>

</div>

        <h3>
            For more information please check on website <a href="https://service-imsp.com/qr-inventory">QRCode inventory website https//service-imsp.com/qr-inventory</a>
        </h3>

</body>

</html>