<?php
	class Dashboard_model extends CI_Model{
        
        public function get_item_list_by_whse($whse){
            $sql = "select count(*) as number_item from v_item_whse where whse_center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        //SELECT COUNT(*) as number FROM `v_item_transaction` where transaction_type = 'r' 

        public function get_alert_lower_minimum_by_whse($whse){
            $sql = "SELECT COUNT(*) as number_lower FROM t_item_whse 
            left outer join t_service_center on t_service_center.id_center = t_item_whse.id_center
            where qty_on_hand <= qty_minimum and t_service_center.center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_receive_transaction_by_whse($whse){
            $sql = "SELECT COUNT(*) as number_receive FROM v_item_transaction where transaction_type = 'r' and center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }


        public function get_issue_transaction_by_whse($whse){
            $sql = "SELECT COUNT(*) as number_issue FROM v_item_transaction where transaction_type = 'i' and center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_all_transaction_by_whse($whse){
            $sql = "SELECT COUNT(*) as number_transaction FROM v_item_transaction where center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_transfer_out_pending_by_whse($whse){
            $sql = "SELECT count(*) as number_transfer_out_pending 
            FROM t_transfer where received_status = ' '
            and from_whse = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_transfer_receive_pending_by_whse($whse){
            $sql = "SELECT count(*) as number_transfer_receive_pending 
            FROM t_transfer where received_status = ' '
            and to_whse = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }


        // admin select data to show dashoboad list

        public function get_alert_lower_minimum_all_whse(){
            $sql = "SELECT t_service_center.center_code, COUNT(*) as number_lower_stock FROM t_item_whse 
            left outer join t_service_center on t_service_center.id_center = t_item_whse.id_center
            where qty_on_hand <= qty_minimum 
            group by t_service_center.center_code";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_alert_count_lower_minimum_all_whse(){
            $sql = "SELECT  COUNT(*) as number_lower_stock FROM t_item_whse 
            left outer join t_service_center on t_service_center.id_center = t_item_whse.id_center
            where qty_on_hand <= qty_minimum 
            ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_count_transfer_receive_pending_all_whse(){
            $sql = "SELECT count(*) as number_transfer_receive_pending 
            FROM t_transfer where received_status = ' '
           
            ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        
        public function get_transfer_receive_pending_all_whse(){
            $sql = "SELECT to_whse,count(*) as number_transfer_receive_pending 
            FROM t_transfer where received_status = ' '
        
            group by to_whse
            ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

    
    }

?>