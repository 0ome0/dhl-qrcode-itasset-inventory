<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ServiceCenter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		$this->load->model('admin/Service_center_model', 'Service_center_model');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin/layout-service-center.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function service_center_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_service_center');
			$crud->set_relation('center_status','t_status','status');
			$crud->display_as('center_status','status');
			$crud->set_subject('Service Center');

			// $crud->required_fields('lastName');

			$crud->set_field_upload('center_map','assets/uploads/files');
			// $crud->field_type('center_code','readonly');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function offices_management()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('offices');
			$crud->set_subject('Office');
			$crud->required_fields('city');
			$crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function service_center_list(){

		$data['active_service_center'] =  $this->Service_center_model->get_active_service_center();
		
		$this->load->view('admin/layout-service-center-list.php',$data);
		
	}

	public function change_service_center(){
		
		$center_code = $this->uri->segment(4);
		// $center_code = 'APD';
		$admin_data = array(
			
			 'whse' => $center_code
		);

		$this->session->set_userdata($admin_data);
		redirect(base_url('admin/dashboard/dashboard_admin'), 'refresh');
	}

}
