<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin/layout-transfer-out.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function transfer_out_pending()
	{

		$whse = $this->session->userdata('whse');
		$transfer_staus = "Pending";
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('t_transfer.from_whse',$whse);
			$crud->where('t_transfer.transfer_status',$transfer_staus);
			$crud->where('t_transfer.received_status','');
			$crud->set_table('t_transfer');
			$crud->order_by('trans_date','desc');

			
			// relation table
			// v_item_whse
			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);
			// t_service center
			$crud->set_primary_key('center_code','t_service_center');
			$crud->set_relation('to_whse','t_service_center','{center_code} - {center_name}',array('center_code <>' => $whse));
			
			$crud->set_primary_key('type_code','t_transtype_code');
			$crud->set_relation('trans_type_code','t_transtype_code','type_description');
			$crud->display_as('trans_type_code','Transfer Item');


			$crud->required_fields('item_id','qty_transfer','to_whse');

			$crud->display_as('item_id','Item Code');

			// unset column
			$crud->unset_add_fields(array('trans_date'));
			$crud->unset_edit();
			

			// Hidden Field
			$crud->field_type('type_code','hidden');
			// $crud->field_type('trans_type_code','hidden');
			$crud->field_type('item_code','hidden');
			$crud->field_type('item_um','hidden');
			$crud->field_type('qty_received','hidden');
			$crud->field_type('transfer_status','hidden');
			$crud->field_type('received_status','hidden');
			$crud->field_type('received_date','hidden');
			$crud->field_type('username_received','hidden');


			$crud->columns('trans_type_code','trans_date','item_id','qty_transfer','from_whse','to_whse','ref_document','username','transfer_status');	

			// call back function to setup data
			//set username
			$crud->callback_add_field('username', function () {
                return ' <input type="text" name="username" value="'.ucwords($this->session->userdata('name')).'" maxlength="50" readonly > ';
			});

			//set service center
			$crud->callback_add_field('from_whse', function () {
				// return ' <input id="field-center_code" class="form-control" name="center_code" value="'.ucwords($this->session->userdata('whse')).'" maxlength="50" type="text" readonly> ';
				return ' <input type="text" name="from_whse" value="'.ucwords($this->session->userdata('whse')).'" maxlength="50" readonly> ';
			});

			//set transaction type = 'ti' (Transfer Out)
			$crud->callback_add_field('trans_type_code', function () {
                return ' <input type="hidden" maxlength="20" value="Transfer Out" name="trans_type_code"> ';
			});

			$crud->callback_before_insert(array($this,'set_trans_type'));

			
			// render view
			$crud->set_subject('Tranfer Status Pending');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-transfer-out.php',(array)$output);
	}

	function set_trans_type($post_array) {
		
		
		$post_array['trans_type_code'] = 'to';
		$transaction_type = $post_array['trans_type_code'];

		$post_array['transfer_status'] = 'Pending';
		$transaction_type = $post_array['transfer_status'];

		// set qty transfer = qty receive
		$post_array['qty_received'] = $post_array['qty_transfer'];
		
		return $post_array;
	}

	public function transfer_out_completed(){
		$whse = $this->session->userdata('whse');
		$transfer_staus = "Pending";
		$received_staus = "Received";
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('t_transfer.from_whse',$whse);
			$crud->where('t_transfer.transfer_status',$transfer_staus);
			$crud->where('t_transfer.received_status',$received_staus);
			$crud->set_table('t_transfer');
			$crud->order_by('trans_date','desc');

			
			// relation table
			// v_item_whse
			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);
			// t_service center
			$crud->set_primary_key('center_code','t_service_center');
			$crud->set_relation('to_whse','t_service_center','{center_code} - {center_name}',array('center_code <>' => $whse));
			
			$crud->set_primary_key('type_code','t_transtype_code');
			$crud->set_relation('trans_type_code','t_transtype_code','type_description');
			$crud->display_as('trans_type_code','Transfer Item');

			
			$crud->required_fields('item_id','qty_transfer','to_whse');

			$crud->display_as('item_id','Item Code');

			// unset column
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			

			// Hidden Field
			$crud->field_type('type_code','hidden');
			// $crud->field_type('trans_type_code','hidden');
			$crud->field_type('item_code','hidden');
			$crud->field_type('item_um','hidden');
			// $crud->field_type('qty_received','hidden');
			$crud->field_type('transfer_status','hidden');
			// $crud->field_type('received_status','hidden');
			// $crud->field_type('received_date','hidden');
			// $crud->field_type('username_received','hidden');


			$crud->columns('trans_type_code','trans_date','item_id','qty_transfer','from_whse','to_whse','qty_received','received_date','username_received','received_status');	



			
			// render view
			$crud->set_subject('Tranfer Status Pending');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-transfer-out-completed.php',(array)$output);
	}

	/////// transfer (in) received 

	public function transfer_receive_pending()
	{

			$whse = $this->session->userdata('whse');
			$transfer_staus = "Pending";
			$receive_status = "Received";
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('t_transfer.to_whse',$whse);
			$crud->where('t_transfer.transfer_status',$transfer_staus);
			$crud->where('t_transfer.received_status','');
			$crud->set_table('t_transfer');
			$crud->set_primary_key('transfer_id');
			$crud->order_by('trans_date','desc');

			
			// relation table
			// v_item_whse
			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);
			// t_service center
			$crud->set_primary_key('center_code','t_service_center');
			$crud->set_relation('to_whse','t_service_center','{center_code} - {center_name}');
			
			$crud->set_primary_key('type_code','t_transtype_code');
			$crud->set_relation('trans_type_code','t_transtype_code','type_description');
			$crud->display_as('trans_type_code','Transfer Type');


			$crud->required_fields('qty_received','received_status');

			$crud->display_as('item_id','Item Code');

			// unset column
			$crud->unset_add_fields(array('trans_date'));
			$crud->unset_add();
			// $crud->unset_edit();
			$crud->unset_delete();
			

			// Hidden Field
			$crud->field_type('type_code','hidden');
			// $crud->field_type('trans_type_code','hidden');
			$crud->field_type('item_code','hidden');
			$crud->field_type('item_um','hidden');
			// $crud->field_type('qty_received','hidden');
			$crud->field_type('transfer_status','hidden');
			$crud->field_type('username','hidden');
			$crud->field_type('transfer_id','hidden');
			// $crud->field_type('received_status','hidden');

			// Readonly Field
			
			$crud->field_type('trans_type_code','readonly');
			$crud->field_type('trans_date','readonly');
			$crud->field_type('item_id','readonly');
			$crud->field_type('qty_transfer','readonly');
			$crud->field_type('from_whse','readonly');
			$crud->field_type('to_whse','readonly');
			$crud->field_type('ref_document','readonly');
			$crud->field_type('note','readonly');
			// $crud->field_type('username','readonly');


			$crud->columns('transfer_id','trans_type_code','trans_date','item_id','qty_transfer','qty_received','from_whse','to_whse','ref_document','username','transfer_status');	

			// call back function to setup data
			//set username received
			$crud->callback_edit_field('username_received', function () {
                return ' <input type="text" name="username_received" value="'.ucwords($this->session->userdata('name')).'" maxlength="50" readonly > ';
			});
			//set received status
			$crud->callback_edit_field('received_status', function () {
				$receive_status = "Received";
                return ' <input type="text" name="received_status" value="'.$receive_status.'" maxlength="50" readonly >  ';
			});
			//set received date
			$crud->callback_edit_field('received_date', function () {
				date_default_timezone_set('Asia/Bangkok');
				// $rcv_date = date("d/m/y H:i:s"); 
				$date = date('d-m-Y H:i:s', time());

                return ' <input type="text" name="received_date" value="'.$date.'" maxlength="50" readonly > ';
			});

			//https://www.grocerycrud.com/documentation/options_functions/callback_after_update
			$crud->callback_after_update(array($this, 'update_transaction'));

			
			// render view
			$crud->set_subject('Tranfer Receive Status Pending');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-transfer-in.php',(array)$output);
	}

	// for admin view 
	public function transfer_receive_pending_all()
	{

			$whse = $this->session->userdata('whse');
			$transfer_staus = "Pending";
			$receive_status = "Received";
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			// $crud->where('t_transfer.to_whse',$whse);
			$crud->where('t_transfer.transfer_status',$transfer_staus);
			$crud->where('t_transfer.received_status','');
			$crud->set_table('t_transfer');
			$crud->set_primary_key('transfer_id');
			$crud->order_by('trans_date','desc');

			
			// relation table
			// v_item_whse
			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);
			// t_service center
			$crud->set_primary_key('center_code','t_service_center');
			$crud->set_relation('to_whse','t_service_center','{center_code} - {center_name}');
			
			$crud->set_primary_key('type_code','t_transtype_code');
			$crud->set_relation('trans_type_code','t_transtype_code','type_description');
			$crud->display_as('trans_type_code','Transfer Type');


			$crud->required_fields('qty_received','received_status');

			$crud->display_as('item_id','Item');

			// unset column
			$crud->unset_add_fields(array('trans_date'));
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			
			$crud->columns('trans_type_code','trans_date','item_id','qty_transfer','from_whse','to_whse','ref_document','note','receive_status','username');

			// Hidden Field
			$crud->field_type('type_code','hidden');
			// $crud->field_type('trans_type_code','hidden');
			$crud->field_type('item_code','hidden');
			$crud->field_type('item_um','hidden');
			// $crud->field_type('qty_received','hidden');
			$crud->field_type('transfer_status','hidden');
			$crud->field_type('username','hidden');
			$crud->field_type('transfer_id','hidden');
			// $crud->field_type('received_status','hidden');

			// Readonly Field
			
			$crud->field_type('trans_type_code','readonly');
			$crud->field_type('trans_date','readonly');
			$crud->field_type('item_id','readonly');
			$crud->field_type('qty_transfer','readonly');
			$crud->field_type('from_whse','readonly');
			$crud->field_type('to_whse','readonly');
			$crud->field_type('ref_document','readonly');
			$crud->field_type('note','readonly');
			// $crud->field_type('username','readonly');


			
			
			// render view
			$crud->set_subject('Tranfer Receive Status Pending');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-transfer-in.php',(array)$output);
	}



	public function update_transaction($post_array,$primary_key){
		// insert transaction table and update qty stock onhand
		// 1. create view v_transfer structure as t_transcation
			// v_transfer
		// 2. select where primary from form = id of view
		// 3. insert into t_transaction

		 $sql = "INSERT INTO t_transaction (item_id, transaction_type, type_reason, qty,note,ref_document,trans_date,center_code,username, transfer_id_ref)
		  SELECT item_id, trans_type, type_reason, qty,note,ref_document,trans_date,center_code,username, transfer_id_ref
		  FROM v_transfer
		  WHERE transfer_id_ref = '$primary_key';
		  ";
		  $query = $this->db->query($sql);

				// check insert complete before update stock 
				// count t_transaction by primary_key if > 0 then run query update  
				$sql_update_stock = "UPDATE t_item_whse as t1
				JOIN v_item_on_hand as t2 ON t1.item_code = t2.item_id and t1.id_center = t2.id_center
				SET t1.qty_on_hand = t2.qty_balance ;
				";
				$query_stock = $this->db->query($sql_update_stock);

		

		
		// 4. qty_qty_stock onhand table t_item_whse

		return $post_array;
	}

	public function transfer_receive_completed()
	{

			$whse = $this->session->userdata('whse');
			$transfer_staus = "Pending";
			$receive_status = "Received";
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('t_transfer.to_whse',$whse);
			$crud->where('t_transfer.transfer_status',$transfer_staus);
			$crud->where('t_transfer.received_status',$receive_status);
			$crud->set_table('t_transfer');
			$crud->set_primary_key('transfer_id');
			$crud->order_by('trans_date','desc');

			
			// relation table
			// v_item_whse
			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);
			// t_service center
			$crud->set_primary_key('center_code','t_service_center');
			$crud->set_relation('to_whse','t_service_center','{center_code} - {center_name}');
			
			$crud->set_primary_key('type_code','t_transtype_code');
			$crud->set_relation('trans_type_code','t_transtype_code','type_description');
			$crud->display_as('trans_type_code','Transfer Item');


			$crud->required_fields('qty_received','received_status');

			$crud->display_as('item_id','Item Code');

			// unset column
			$crud->unset_add_fields(array('trans_date'));
			$crud->unset_add();
			// $crud->unset_edit();
			$crud->unset_delete();
			

			// Hidden Field
			$crud->field_type('type_code','hidden');
			// $crud->field_type('trans_type_code','hidden');
			$crud->field_type('item_code','hidden');
			$crud->field_type('item_um','hidden');
			// $crud->field_type('qty_received','hidden');
			$crud->field_type('transfer_status','hidden');
			$crud->field_type('username','hidden');
			$crud->field_type('transfer_id','hidden');
			// $crud->field_type('received_status','hidden');

			// Readonly Field
			
			$crud->field_type('trans_type_code','readonly');
			$crud->field_type('trans_date','readonly');
			$crud->field_type('item_id','readonly');
			$crud->field_type('qty_transfer','readonly');
			$crud->field_type('from_whse','readonly');
			$crud->field_type('to_whse','readonly');
			$crud->field_type('ref_document','readonly');
			$crud->field_type('note','readonly');
			// $crud->field_type('username','readonly');


			$crud->columns('transfer_id','trans_type_code','trans_date','item_id','qty_transfer','qty_received','from_whse','to_whse','ref_document','username','received_status');	

			// call back function to setup data
			//set username received
			$crud->callback_edit_field('username_received', function () {
                return ' <input type="text" name="username_received" value="'.ucwords($this->session->userdata('name')).'" maxlength="50" readonly > ';
			});
			//set received status
			$crud->callback_edit_field('received_status', function () {
				$receive_status = "Received";
                return ' <input type="text" name="received_status" value="'.$receive_status.'" maxlength="50" readonly >  ';
			});
			//set received date
			$crud->callback_edit_field('received_date', function () {
				date_default_timezone_set('Asia/Bangkok');
				// $rcv_date = date("d/m/y H:i:s"); 
				$date = date('d-m-Y H:i:s', time());

                return ' <input type="text" name="received_date" value="'.$date.'" maxlength="50" readonly > ';
			});

			//https://www.grocerycrud.com/documentation/options_functions/callback_after_update
			$crud->callback_after_update(array($this, 'update_transaction'));

			
			// render view
			$crud->set_subject('Tranfer Receive Status Pending');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-transfer-in.php',(array)$output);
	}
				
}
			