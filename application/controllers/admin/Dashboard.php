<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends MY_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('itasset/dashboard_model', 'dashboard_model');
		}

		public function dashboard_admin (){
			$whse = $this->session->userdata('whse');
			$uesr_role = $this->session->userdata('role');

			redirect(base_url('admin/dashboard/dashboard_user'), 'refresh');

			// if($uesr_role == '2') {
			// 	redirect(base_url('admin/dashboard/dashboard_user'), 'refresh');
			// }

			// $data['alert_count_lower_minimum_all_whse'] =  $this->dashboard_model->get_alert_count_lower_minimum_all_whse();
			// $data['alert_lower_minimum_all_whse'] =  $this->dashboard_model->get_alert_lower_minimum_all_whse();
			// $data['count_transfer_receive_pending_all_whse'] =  $this->dashboard_model->get_count_transfer_receive_pending_all_whse();
			// $data['transfer_receive_pending_all_whse'] =  $this->dashboard_model->get_transfer_receive_pending_all_whse();
			
			

			// $this->load->view('admin/layout-dashboard-admin.php',$data);
		}

		public function dashboard_location (){
			$whse = $this->session->userdata('whse');
			$uesr_role = $this->session->userdata('role');

			
			$data['location_item_asset'] =  $this->dashboard_model->get_location_item_asset();$data['location_item_asset'] =  $this->dashboard_model->get_location_item_asset();
			$data['number_item_asset'] =  $this->dashboard_model->get_number_item_asset_by_location();



			$this->load->view('itasset/layout-dashboard-by-location.php',$data);
		}

		public function dashboard_user (){
			$whse = $this->session->userdata('whse');
			$uesr_role = $this->session->userdata('role');

			

			// $data['items_by_whse'] =  '1';//$this->dashboard_model->get_item_list_by_whse($whse);
			// $data['alert_lower_minimum_by_whse'] = '1';// $this->dashboard_model->get_alert_lower_minimum_by_whse($whse);
			// $data['receive_transaction_by_whse'] =  '1';//$this->dashboard_model->get_receive_transaction_by_whse($whse);
			// $data['issue_transaction_by_whse'] = '1';// $this->dashboard_model->get_issue_transaction_by_whse($whse);
			// $data['all_transaction_by_whse'] =  '1';//$this->dashboard_model->get_all_transaction_by_whse($whse);
			// $data['transfer_out_pending_by_whse'] =  '1';//$this->dashboard_model->get_transfer_out_pending_by_whse($whse);
			// $data['transfer_receive_pending_by_whse'] =  '1';//$this->dashboard_model->get_transfer_receive_pending_by_whse($whse);

			$data['unassign_owner'] =  $this->dashboard_model->get_unassign_owner();
			$data['assign_owner'] =  $this->dashboard_model->get_assign_owner();
			$data['asset_item'] =  $this->dashboard_model->get_itasset();
			$data['asset_owner'] =  $this->dashboard_model->get_asset_owner();
			$data['asset_expired'] =  $this->dashboard_model->get_asset_expired();
			$data['asset_expired_1'] =  $this->dashboard_model->get_asset_expired_in_1();
			$data['asset_expired_2'] =  $this->dashboard_model->get_asset_expired_in_2();
			$data['asset_expired_3'] =  $this->dashboard_model->get_asset_expired_in_3();

			$data['vendor'] =  $this->dashboard_model->get_vendor();

			$data['status_item_asset'] =  $this->dashboard_model->get_usage_staus_item_asset();
			$data['asset_type_item_asset'] =  $this->dashboard_model->get_asset_type_item_asset();


			

			$this->load->view('itasset/layout-dashboard-center.php',$data);
		}


		// public function index(){
		// 	$data['view'] = 'admin/dashboard/index';
		// 	$this->load->view('admin/layout', $data);
		// }

		// public function index2(){
		// 	$data['view'] = 'admin/dashboard/index2';
		// 	$this->load->view('admin/layout', $data);
		// }
	}

?>	