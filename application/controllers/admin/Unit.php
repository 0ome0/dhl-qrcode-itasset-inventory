<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin/layout-unit.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function unit_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_unit');
			// $crud->set_relation('center_status','t_status','status');
			// $crud->display_as('center_status','status');
			$crud->set_subject('Unit');

			// $crud->required_fields('lastName');

			// $crud->set_field_upload('center_map','assets/uploads/files');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function get_item_unit($item_id)
	{
	
			//http://localhost/inventory/admin/unit/get_item_unit/2
			//change talble to view
			$query = $this->db->query("SELECT * FROM v_item_unit where item_id = '$item_id' order by base_quantity ");
				
			
			if( $query->num_rows() > 0) {
				$result = $query->result(); //or $query->result_array() to get an array
				echo "<select id='field_unit_chosen' name = 'unit'>";
				foreach( $result as $row )
				{
					echo 	"<option value='".$row->item_um_code."'>".$row->unit.' / '.$row->base_quantity." unit</option>";
					
				}
				echo "</select>";
			}
			
	}

	public function get_qty_convert($item_id , $item_um_code)

	{
		$query = $this->db->query("SELECT * FROM v_item_unit where item_id = '$item_id' and item_um_code = '$item_um_code'");

		if( $query->num_rows() > 0) {
			$result = $query->result(); //or $query->result_array() to get an array
			
			foreach( $result as $row )
			{
				// echo 	"<input type='text' value = '".$row->base_quantity."'>";
				echo	$row->base_quantity;
				
			}
			
		}
	}

}
