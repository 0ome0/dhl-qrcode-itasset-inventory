<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	public function _example_output($output = null)
	{
		$this->load->view('itasset/layout-company.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function company_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_company');
			// $crud->set_relation('vendor_status','t_status','status',null,'status desc');
			$crud->display_as('comp_email','Email Address');
			$crud->set_subject('Email Setting');

			$crud->required_fields('comp_email');
        
            $crud->columns('comp_email');
            $crud->fields('comp_email');
            $crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			$this->_example_output($output);
	}

}
