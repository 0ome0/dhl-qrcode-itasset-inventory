<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	public function _example_output($output = null)
	{
		$this->load->view('itasset/layout-vendor.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function vendor_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_vendor');
			$crud->set_relation('vendor_status','t_status','status',null,'status desc');
			$crud->display_as('status','status');
			$crud->set_subject('Vendor');

			$crud->required_fields('vendor_name');
			$crud->required_fields('vendor_status');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			$this->_example_output($output);
	}

}
