<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qr_scan extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->model('admin/auth_model', 'auth_model');
        // $this->load->model('admin/transaction_model', 'transaction_model');
        $this->load->model('itasset/Item_asset_model', 'Item_asset_model');

    }

	public function index()
	{
		redirect(base_url('admin'));
    }
    
    public function scan(){
        $qr_code_id  = $this->input->get('qr_code_id');


        $result = $this->Item_asset_model->get_item_asset_detail($qr_code_id);
      
        if ($result == TRUE) {
            // echo json_encode($result);

            // $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
            // $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);
            $asset_type = $result[0]['asset_type'];

            $this->alert_line_notify($qr_code_id);

            $data['view'] = $asset_type;
            $data['item_detail'] = $result;

            // $this->load->view("itasset/layout-qr_result.php", $data);
            $this->load->view("itasset/layout-qr_result_box.php", $data);
            // echo $data['view'] ;


        }
        else{
            $msg = 'Not Found Data';
            echo $msg;
        }

    } 

    // public function login(){

    //     $username = $this->input->get('username');
    //     $password = $this->input->get('password');
        
    //     $data = array(
    //         'username' => $username,
    //         'password' => $password 
    //         );

    //         $result = $this->auth_model->login($data);
    //         if ($result == TRUE) {
    //             $admin_data = array(
    //                 'admin_id' => $result['id'],
    //                  'name' => $result['username'],
    //                  // add warehouse and user profile set session
    //                  'whse' => $result['center_code'],
    //                  'email' => $result['email'],
    //                  'firstname' => $result['firstname'],
    //                  'lastname' => $result['lastname'],
    //                  'role' => $result['role']
    //                 //  'is_admin_login' => TRUE
    //             );

    //             echo json_encode($admin_data);
    //         }
    //         else{
    //             $msg = 'Invalid Email or Password!';
    //             echo $msg;
    //         }
    // }

    public function gen_newqr(){

        $start_asset_id = 1;


        
        for($i=1;$i<=400;$i++)
        
        {
       
            $id  =  $i;
        
            $result = $this->Item_asset_model->get_item_by_id_and_not_have_qr($id);
            $data['item_detail'] = $result;

            if ($result == TRUE) {
                
                echo $i;
                echo "</br>";
                $qr_code = strtoupper(uniqid());
                // $barcode_id = '*'.$barcode.'*';
                

				$this->load->library('ciqrcode');

                //$qr_gen = base_url()."api/qr_scan/scan?qr_code_id=".$qr_code;
                $qr_gen = "https://service-imsp.com/qr-inventory/api/qr_scan/scan?qr_code_id=".$qr_code;

				// $params['data'] = $qr_code;
				$params['data'] = $qr_gen;
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = "assets/images/qrcode/$qr_code.png";

				$qrcode_image = "$qr_code.png" ;
                $this->ciqrcode->generate($params);
                
                // to save 
                $qr_code_id = $qr_code;
                $qr_code_img = $qrcode_image;
                $asset_id = $i;
                
                $sql = "update t_item_asset 
                set qr_code_id = '$qr_code_id' , qr_code_img = '$qr_code_img'
                where asset_id = '$asset_id' ";
			    $query = $this->db->query($sql);

            }
            else{
                $msg = '';
                //echo $id; 
                echo $msg;
            }

        
        }


                   
    } 

    public function alert_line_notify($qr_code_id){

        $Token = 'lLeKyLK3tfdHKpKqK17f9Nfn0epTPEcLFPDr1RzLwzw';
        $from = 'DHL IT Inventory';

        date_default_timezone_set("Asia/Bangkok");
        $scan_date = date("Y-m-d H:i:s");
        
        // call line api
        $message = "มีการ Scan QR Code $qr_code_id จากระบบ : $from วันที่ $scan_date";

        // echo $message;

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        // SSL USE 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        //POST 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //Check error 
        if(curl_error($chOne)) 
        { 
            // echo 'error:' . curl_error($chOne); 
        } 
        else { 
        $result_ = json_decode($result, true); 
        // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        // redirect('/menu_list/ordered','refresh');
            } 
        curl_close( $chOne );

    }
    
    
}
