-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 01, 2018 at 05:17 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devdeeth_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `center_code` varchar(5) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `center_code`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'admin@admin.com', 'RMT', 'System', 'Admin', 'admin@admin.com', '12345', '$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS', 1, 1, 1, '', '2017-09-29 10:09:44', '2017-10-17 10:10:55'),
(30, 'test@test.com', 'APD', 'demo_user', 'test_user', 'test@test.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 2, 1, 1, '', '2017-12-06 04:12:59', '2018-01-07 02:01:15'),
(31, 'test2@test2.com', 'RMT', 'User2', 'Demo', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 1, '', '2018-01-07 01:01:32', '2018-01-07 01:01:32'),
(146, 'TH1789', 'APD', 'Kunchorn', 'Chaiyanon', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'TH1134', 'APD', 'Attapol', 'Kongsin', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'TH0624', 'APD', 'Worawut', 'Yotboonreung', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'TH2971', 'CXM', 'Paiboon', 'Sumchaiwong', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'TH2298', 'CXM', 'Niwest', 'Mesomroj', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'TH1879', 'CXM', 'Choosak', 'Somsri', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'TH1096', 'CXM', 'Thawat', 'Thikeaw', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'TH3169', 'EGW', 'Attapon', 'Permpoonkumpon', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'TH2802', 'LZB', 'Dumrong', 'Pavapootanont', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'TH2323', 'LZB', 'Sittichai', 'Khunsri', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'TH1433', 'LZB', 'Amnuay', 'Thachai', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'TH1202', 'LZB', 'Suchart', 'Arntaso', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'TH1154', 'LZB', 'Wanchai', 'Thipkoy', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'TH0849', 'LZB', 'Prathoung', 'Sroythong', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'TH0505', 'LZB', 'Supoj', 'Ploykerd', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'TH2606', 'NBK', 'Chachaisawat', 'Sarachan', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'TH2590', 'NBK', 'Kittisak', 'Trimuck', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'TH1830', 'RMT', 'Surin', 'Nakaiam', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'TH1651', 'RMT', 'Chana', 'Piamthanaphaibun', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'TH1053', 'TZB', 'Wanchai', 'Boonlert', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'TH0869', 'TZB', 'Yuthaporn', 'Noppakunsawad', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'TH0379', 'TZB', 'Surapol', 'Dulyasith', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'TH0276', 'TZB', 'Somchai', 'Sanyakhun', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'TH0994', 'ZVB', 'Yotdanai', 'Udomrach', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'TH3437', 'LZN', 'Teerapong', 'Jinokoon', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'TH2346', 'APD', 'Nuttapong', 'Chareunsong', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'TH2093', 'APD', 'Nattee', 'Toma', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'TH0447', 'APD', 'Samarn', 'Jaitha', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'TH0273', 'APD', 'Denchai', 'Sermkitrungrueng', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'TH1995', 'EGW', 'Chalermporn', 'Muangklom', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'TH1360', 'EGW', 'Surayos', 'Pimpasuraka', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'TH1333', 'EGW', 'Tuantong', 'Rainpricha', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'TH0997', 'EGW', 'Prapol', 'Chitcharoenporn', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'TH0975', 'EGW', 'Surasak', 'Tarason', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'TH3114', 'LZB', 'Somporn', 'Kunratsee', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'TH2225', 'NBK', 'Wutthichai', 'Srikote', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'TH1887', 'NBK', 'Sigkharin', 'Wanngern', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'TH1416', 'NBK', 'Kumpol', 'Siriwat', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'TH1071', 'NKR', 'Pisit', 'Pongpisarn', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'TH0719', 'PCB', 'Boonsom', 'Prasert', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'TH1286', 'RMT', 'Montri', 'Viriyachaimongkol', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'TH0715', 'RMT', 'Yuttasak', 'Boonprasong', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'TH0134', 'RMT', 'Varakorn', 'Pitjarana', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'TH0121', 'RMT', 'Manit', 'Kengrathok', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'TH0065', 'ZVB', 'Santi', 'Ngamketkamol', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'TH1969', 'GDR', 'Somchai', 'Chaimars', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'TH1552', 'GDR', 'Suradech', 'Suttharat', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'TH1203', 'GDR', 'Kittikorn', 'Lomnetr', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'TH0030', 'GDR', 'Bancha', 'Rodlertrai', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, '36700739', 'LZN', 'Nuttapol', 'Thakha', '', '', '$2y$10$G.FArcP2In6/I7koPjXlAu4rxSDjPQ7gEJ7FZ9U6mQoQ/uUkfZW8.', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
