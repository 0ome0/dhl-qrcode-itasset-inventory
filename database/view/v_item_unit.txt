create view v_item_unit AS
SELECT t3.id_item as item_id ,t3.item_code , t3.item_um_code,  t4.unit, t4.base_quantity
from t_item t3
left outer join t_unit t4 on t3.item_um_code = t4.item_um_code

union ALL

select t1.item_code as item_id , t5.item_code , t1.id_unit , t2.unit, t2.base_quantity
from t_item_unit t1 
left outer join t_unit t2 on t1.id_unit = t2.item_um_code
left outer join t_item t5 on t1.item_code = t5.id_item