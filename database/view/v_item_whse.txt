alter view v_item_whse AS

select t1.id, t1.item_code as item_id , t1.id_center , t2.id_item, t2.item_description ,
t2.item_code ,
t3.center_code as whse_center_code
, t1.qty_on_hand , t1.shelf_location

from t_item_whse as t1
inner join t_item as t2 on t1.item_code = t2.id_item
inner join t_service_center as t3 on t1.id_center = t3.id_center


version 2
select `t1`.`id` AS `id`,`t1`.`item_code` AS `item_id`,`t1`.`id_center` AS `id_center`
,`t2`.`id_item` AS `id_item`,`t2`.`item_description` AS `item_description`
,`t2`.`item_code` AS `item_code`
,`t3`.`center_code` AS `whse_center_code` 
, t1.qty_on_hand , t1.shelf_location 
from ((`t_item_whse` `t1` join `t_item` `t2` on((`t1`.`item_code` = `t2`.`id_item`))) 
join `t_service_center` `t3` on((`t1`.`id_center` = `t3`.`id_center`)))

version 3
select `t1`.`id` AS `id`,`t1`.`item_code` AS `item_id`,`t1`.`id_center` AS `id_center`
,`t2`.`id_item` AS `id_item`,`t2`.`item_description` AS `item_description`
,`t2`.`item_code` AS `item_code`
,`t3`.`center_code` AS `whse_center_code` 
, t1.qty_on_hand , t1.shelf_location  , t1.qty_minimum , t1.lead_time_day
from ((`t_item_whse` `t1` join `t_item` `t2` on((`t1`.`item_code` = `t2`.`id_item`))) 
join `t_service_center` `t3` on((`t1`.`id_center` = `t3`.`id_center`)))