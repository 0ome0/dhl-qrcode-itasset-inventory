V1
select t1.id, t1.item_id , t1.transaction_type, t1.type_reason, t1.qty , 
t1.trans_date , t2.item_barcode, t2.item_description, t2.item_code, 
t2.item_um_code, t2.qty_stock_minimun , t2.item_image ,t3.unit 
,t4.issue_reason as reason
from t_transaction as t1 left join t_item as t2 
on t1.item_id = t2.id_item left join t_unit t3 
on t2.item_um_code = t3.item_um_code
left join v_transaction_type as t4
on t1.transaction_type = t4.trans_type and t1.type_reason = t4.id

V2
alter VIEW v_item_transaction as 

select t1.id, t1.trans_date, t1.transaction_type , t1.type_reason , t1.item_id , t1.qty , t1.note, t1.ref_document , t1.center_code , t1.username ,
t2.item_barcode , t2.item_description , t2.item_code , t2.item_um_code , t2.item_type_id,
t3.center_name,
t4.issue_reason as reason , t4.type_description

from t_transaction as t1 
left outer  join t_item as t2 on t1.item_id = t2.id_item 
left outer  join t_service_center as t3 on t1.center_code = t3.center_code 
left outer join v_transaction_type as t4 on t1.transaction_type = t4.trans_type and t1.type_reason = t4.id