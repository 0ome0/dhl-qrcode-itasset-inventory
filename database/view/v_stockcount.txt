create view v_stockcount as 

SELECT t1.id_item as item_id , 'adj' as transaction_type , '5' as type_reason

, t2.qty_on_hand , t1.qty_count

, (t1.qty_count - t2.qty_on_hand) as qty , '' as note , '' as ref_document
, now() as trans_date , t1.whse_center_code as center_code , t1.username 
, t1.id as transfer_id_ref
FROM `t_stockcount` as t1 
left outer  join t_item_whse t2 
on t1.id_item = t2.item_code and t1.id_center = t2.id_center
where t1.count_status = '2'