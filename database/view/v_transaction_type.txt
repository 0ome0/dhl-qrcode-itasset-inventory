alter view v_transaction_type as
select  t1.trans_type  AS  trans_type , t1.id  AS  id , t1.issue_reason  AS  issue_reason  
,t3.type_description
from  t_issue_reason as t1  
left outer join t_transtype_code as t3 on t1.trans_type = t3.type_code

union all 

select  t2.trans_type  AS  trans_type , t2. id  AS  id , t2.receive_reason  AS  receive_reason  
,t4.type_description
from  t_receive_reason as  t2 
left outer join t_transtype_code as t4 on t2.trans_type = t4.type_code

union all
select t3.type_code as trans_type , t3.id , 'transfer' as issue_reason ,t3.type_description
from t_transtype_code as t3 where t3.type_code like 't%'

union all
select t3.type_code as trans_type , t3.id , 'Physical Count' as issue_reason ,t3.type_description
from t_transtype_code as t3 where t3.type_code like 'adj%'

